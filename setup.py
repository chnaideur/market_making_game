
from setuptools import setup, find_packages

install_requires = [
    'bottle',
    'bottle_websocket',
    'gevent'
]

setup(
    name='market_making_game',
    version='0.1.0',
    packages=find_packages(),
    package_data={ '': ['*.html'] },
    entry_points={
        'console_scripts': [ 'market_making_game=market_making_game.__main__:main' ]
    },
    install_requires=install_requires,
    extras_require={
        'test': [ 'pytest' ]
    }
)
