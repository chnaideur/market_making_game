
import sys
import os
import market_making_game.application_handler

from shutil import copyfile
from bottle import Bottle, static_file
from bottle.ext.websocket import GeventWebSocketServer, websocket

def main():
    for f in os.listdir('.'):
        if f.endswith('jpg') or f.endswith('jpeg') or f.endswith('png'):
            copyfile(f, 'market_making_game/views/media/img/cheese.jpg')

    app_handler = market_making_game.application_handler.ApplicationHandler()
    app = Bottle()

    app.route('/', method='GET', callback=lambda: static_file('index.html', root='market_making_game/views/'))
    app.route('/media/<name>', method='GET', callback=lambda name: static_file('media/' + name, root='market_making_game/views/'))
    app.route('/img/<name>', method='GET', callback=lambda name: static_file('media/img/' + name, root='market_making_game/views/'))
    app.route('/api/<name>', method='GET', callback=app_handler.get_handler)
    app.route('/api/<name>', method='PUT', callback=app_handler.put_handler)
    app.route('/api/<name>', method='POST', callback=app_handler.post_handler)
    app.route('/api/ws_data', method='GET', callback=app_handler.ws_connection, apply=[websocket])
    app.add_hook('after_request', app_handler.send_updated_market_state)
    app.run(host='0.0.0.0', port=8080, server=GeventWebSocketServer)

if __name__ == '__main__':
    sys.exit(main())
