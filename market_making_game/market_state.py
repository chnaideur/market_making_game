#!/usr/bin/env python

import datetime
import collections

class User:
    def __init__(self, name):
        self.name = name
        self.orders = []

class Trade:
    def __init__(self, buyer, seller, price):
        self.buyer = buyer
        self.seller = seller
        self.price = price
        self.when = datetime.datetime.now()

class Order:
    def __init__(self, user, price, isBid):
        self.user = user
        self.price = price
        self.isBid = isBid
        self.created = datetime.datetime.now()

class Level:
    def __init__(self):
        self.bids = []
        self.asks = []

class Book:
    def __init__(self, ticksize):
        self.levels = collections.defaultdict(Level)
        self.ticksize = ticksize

class MarketState:
    def __init__(self, ticksize):
        self.book = Book(ticksize)
        self.trades = []
        self.users = {}



