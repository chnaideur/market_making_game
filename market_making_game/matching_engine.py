
from market_making_game.market_state import Trade

def get_tob(levels):
    best_bid = None
    best_ask = None

    for level_price in sorted(levels.keys()):
        level = levels[level_price]

        assert len(level.bids) == 0 or len(level.asks) == 0

        if len(level.bids) > 0:
            best_bid = level_price

        elif len(level.asks) > 0 and (not best_ask or level_price < best_ask):
            best_ask = level_price

    return (best_bid, best_ask)


def add_order(market_state, order_info):
    """ Receives a MarketState and Order object, checks
        if the new order trades with an order in the book,
        then updates MarketState and returns it """

    levels = market_state.book.levels
    (best_bid, best_ask) = get_tob(levels)

    if order_info.isBid and best_ask and order_info.price >= best_ask:
        # Trading with an existing ask order
        standing_order = levels[best_ask].asks[0]

        market_state.trades.append(
                Trade(
                    buyer=order_info.user,
                    seller=standing_order.user,
                    price=standing_order.price))

        (asks, user) = delete_order(levels[best_ask].asks, market_state.users[standing_order.user])

        levels[best_ask].asks = asks
        market_state.users[standing_order.user] = user

        # Delete level if it is no longer used
        if len(levels[best_ask].asks) == 0:
            del levels[best_ask]

    elif not order_info.isBid and best_bid and order_info.price <= best_bid:
        # Trading with an existing bid order
        standing_order = levels[best_bid].bids[0]

        market_state.trades.append(
                Trade(
                    buyer=standing_order.user,
                    seller=order_info.user,
                    price=standing_order.price))

        (bids, user) = delete_order(levels[best_bid].bids, market_state.users[standing_order.user])

        levels[best_bid].bids = bids
        market_state.users[standing_order.user] = user

        # Delete level if it is no longer used
        if len(levels[best_bid].bids) == 0:
            del levels[best_bid]

    else:
        # No trade, insert the order into the book
        if order_info.isBid:
            levels[order_info.price].bids.append(order_info)
        else:
            levels[order_info.price].asks.append(order_info)

        market_state.users[order_info.user].orders.append(order_info)

    return market_state


def delete_order(orders, user, assert_if_not_found=False):
    """ Delete all orders for the given user, takes
        a list of orders on the level for the given side
        and a User object. Returns the updated orders list
        and user object """

    for (idx, order) in enumerate(orders):
        if order.user == user.name:
            price = order.price
            del orders[idx]
            break
    else:
        if assert_if_not_found:
            assert False, 'no order found for user {}'.format(user.name)
        else:
            return (orders, user)

    for (idx, order) in enumerate(user.orders):
        if order.price == price:
            del user.orders[idx]
            break
    else:
        assert False, 'no order found for user {} of value {}'.format(user.name, price)

    return (orders, user)


def delete_orders(market_state, user):
    """ Receives a MarketState and user object, deletes
        all orders belonging to this user and returns
        the updated market state """

    for price, level in market_state.book.levels.items():
        (level.bids, user) = delete_order(level.bids, user)
        (level.asks, user) = delete_order(level.asks, user)

        # Write the updated state back to the original market state object
        if len(level.bids) > 0 or len(level.asks) > 0:
            market_state.book.levels[price] = level
        else:
            del market_state.book.levels[price]

        market_state.users[user.name] = user

    return market_state


def get_user_pnl(market_state, settlement_price):
    """ Calculates the P/L for each user by looking at the list of trades """

    user_pnl = { user: 0 for user in market_state.users }

    for trade in market_state.trades:
        buyer_profit = get_buyer_trade_pnl(trade, settlement_price)
        user_pnl[trade.buyer] += buyer_profit
        user_pnl[trade.seller] -= buyer_profit

    return user_pnl

def get_buyer_trade_pnl(trade, settlement_price):
    return settlement_price - trade.price
