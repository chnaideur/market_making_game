#!/usr/bin/env python

import json
import datetime
import time
import matching_engine

class ViewController:
    def __init__(self, marketState, thisUser):
        self.marketState = marketState
        self.thisUser = thisUser

    def getTradeList(self):
        tradeList = []

        if self.marketState:
            for trade in self.marketState.trades:
                t = {}
                t["buyer"] = trade.buyer
                t["seller"] = trade.seller
                t["price"] = trade.price
                t["when"] = self._dateTimeToISOWithTZ(trade.when)
                t["isme"] = (trade.buyer == self.thisUser.name or trade.seller == self.thisUser.name) if self.thisUser is not None else False
                tradeList.append(t)

        return tradeList

    def getMyOrders(self):
        orders = {}
        if self.thisUser is None:
            return

        for order in self.thisUser.orders:
            orders["bid" if order.isBid else "ask"] = self._orderToDict(order)
        return orders

    def getBook(self, levels = None):
        b = {}
        b["ticksize"] = self.marketState.book.ticksize
        b["levels"] = []

        if len(self.marketState.book.levels.keys()) == 0:
            return b

        best_disp_bid, best_disp_ask = matching_engine.get_tob(self.marketState.book.levels)

        # Check if there are orders only on one side of the book so
        # that best bid and best ask are always populated
        if best_disp_bid == None:
            best_disp_bid = best_disp_ask - self.marketState.book.ticksize
        elif best_disp_ask == None:
            best_disp_ask = best_disp_bid + self.marketState.book.ticksize

        if levels is not None:
            min_level = best_disp_bid - self.marketState.book.ticksize * levels
            max_level = best_disp_ask + self.marketState.book.ticksize * levels
        elif len(self.marketState.book.levels) > 0:
            # Retrieve all the levels
            min_level = min(self.marketState.book.levels.keys())
            max_level = max(self.marketState.book.levels.keys())
        else:
            # No levels to return
            return b

        min_level = max(min_level, 0)

        # Add the Ask orders
        for price in range(max_level, best_disp_ask - 1, -self.marketState.book.ticksize):
            level = self.marketState.book.levels[price]
            b["levels"].append(self._levelToDict(level, price))

        # Add the Bid orders
        for price in range(best_disp_bid, min_level - 1, -self.marketState.book.ticksize):
            level = self.marketState.book.levels[price]
            b["levels"].append(self._levelToDict(level, price))

        return b

    def getMarketState(self, levels = None):
        s = {}
        s["trade_list"] = self.getTradeList()
        s["my_orders"] = self.getMyOrders()
        s["book"] = self.getBook(levels)
        return s

    def getSettlementDetails(self, price):
        s = {}
        s["by_user"] = self.getSettlementByUser(price)
        s["by_trade"] = self.getSettlementByTrade(price)
        return s

    def getSettlementByUser(self, price):
        users = matching_engine.get_user_pnl(self.marketState, price)
        userList = [{ "user" : user, "pnl" : pnl } for user, pnl in users.items()]
        userList.sort(key= lambda x : x["pnl"], reverse=True)
        return userList

    def getSettlementByTrade(self, price):
        tradeList = []
        for trade in self.marketState.trades:
            t = {}
            t["buyer"] = trade.buyer
            t["seller"] = trade.seller
            t["price"] = trade.price
            t["when"] = self._dateTimeToISOWithTZ(trade.when)
            t["isme"] = (trade.buyer == self.thisUser.name or trade.seller == self.thisUser.name) if self.thisUser is not None else False
            t["pnl_buyer"] = matching_engine.get_buyer_trade_pnl(trade, price)

            tradeList.append(t)

        return tradeList

    def roundUpToTick(self, price):
        rounded = self.roundDownToTick(price)
        if rounded < price:
            return rounded + self.marketState.book.ticksize
        return rounded

    def roundDownToTick(self, price):
        return int(int(float(price) / self.marketState.book.ticksize) * self.marketState.book.ticksize)

    def _levelToDict(self, level, price):
        l = {}
        l["bids"] = []
        l["asks"] = []
        l["price"] = price

        for o in level.bids:
            l["bids"].append(self._orderToDict(o))
        for o in level.asks:
            l["asks"].append(self._orderToDict(o))
        return l

    def _orderToDict(self, order):
        o = {}
        o["user"] = order.user
        o["price"] = order.price
        o["isBid"] = order.isBid
        o["created"] = self._dateTimeToISOWithTZ(order.created)
        return o

    def _timeZoneOffsetMinutes(self):
        ts = time.time()
        utc_offset = (datetime.datetime.fromtimestamp(ts) -
              datetime.datetime.utcfromtimestamp(ts)).total_seconds()
        return int(round(utc_offset / 60))

    def _dateTimeToISOWithTZ(self, dateTime):
        offset_total_min = self._timeZoneOffsetMinutes()
        offset_hours = abs(offset_total_min / 60)
        offset_min = abs(offset_total_min % 60)
        return dateTime.isoformat() + "{}{}:{:02}".format("+" if offset_total_min > 0 else "-", offset_hours, offset_min)

