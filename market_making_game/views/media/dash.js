$(document).ready(function() {
    var orderBookTable;
    var tradesChartData = [];

    init();

    function init() {
        orderBookTable = $('#orderBookTable').DataTable({
            "searching":false,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "columnDefs": [
                {
                    targets: 0,
                    className: 'dt-right no-wrap'
                },
                {
                    targets: 1,
                    className: 'dt-center'
                },
                {
                    targets: 2,
                    className: 'dt-left no-wrap'
                }
            ]
        });

		refreshDash();
/*        var addr = 'ws://' + document.domain + ':' + location.port + '/api/ws_data?levels=10'
        var socket = new WebSocket(addr);

        socket.onerror = function(event) {
            console.log('Error detected: ' + event);
        }

        socket.onclose = function() {
            socket = null;
            console.log('Socket closed')
        }

        socket.onopen = function(event) {
            console.log('Socket open')
        }

        socket.onmessage = function(msg) {
            console.log("Refreshing data.");
            var data = JSON.parse(msg.data)
            console.log(data)
            drawTradesChart(data);
            drawOrderBook(data);
            drawTradesList(data);
        }*/
    }

    function refreshDash() {
        console.log("Refreshing data.");

        $.getJSON("../api/state?levels=10", function(data) {
            console.log(data);
            drawTradesChart(data);
            drawOrderBook(data);
            drawTradesList(data);

            setTimeout(refreshDash, 1000);
        }).fail(function(data) {
            console.log(data);
            setTimeout(refreshDash, 1000);
        });
    }

    function drawOrderBook(state) {
        var levels = state["book"]["levels"];
        var tableData = [];

        levels.forEach(function(level) {
            var bids = level["bids"];
            var asks = level["asks"];
            var bidsText = "";
            var asksText = "";
            var priceText = level["price"];

            bids.reverse().forEach(function(bid) {
                bidsText = bidsText + bid["user"] + " ";
            });

            if (bids.length > 0) bidsText = bidsText + ": " + bids.length;
            if (asks.length > 0) asksText = asksText + asks.length + " :";

            asks.forEach(function(ask) {
                asksText = asksText + " " + ask["user"];
            });

            tableData.push([bidsText, priceText, asksText]);
        });

        if (levels.length == 0) {
            tableData.push(["", "No Orders Placed", ""])
        }

        orderBookTable.clear();
        orderBookTable.rows.add(tableData);
        orderBookTable.draw();
    }

    function drawTradesList(state) {
        var trades = state["trade_list"];
        var tradesText = "";
        trades.forEach(function(trade) {
            tradesText = (tradesText +
                trade["buyer"] + " bought @ " +
                trade["price"] + " from " +
                trade["seller"] + "\n");
        });

        $("#tradesTextArea").text(tradesText);
        $("#tradesTextArea").scrollTop($("#tradesTextArea")[0].scrollHeight);
    }

    function drawTradesChart(state) {
        var trades = state["trade_list"];
        var data = [];
        var labels = [];
        trades.forEach(function(trade) {
            data.push(trade["price"]);
            labels.push("");
        });

        if(data.length === tradesChartData.length && data.every(function(v,i) { return v === tradesChartData[i]})) return;
        tradesChartData = data;

        var ctx = document.getElementById("tradesChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Trade prices',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    }
});

