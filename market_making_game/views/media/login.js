$(document).ready(function() {
    $("#loginButton").click(function() {
        username = $("#usernameInput").val();

        console.log("Logging in");
        
        $.get("../api/login?name=" + username, 
            function(data, status) {
                console.log("Logged in");
                window.location.href = "/media/trader.html";
            }
        ).fail(function(data) {
            alert(data['responseJSON']['error']); 
        });
    });
});

