
$(document).ready(function() {
    init()

    function init() {
        refresh();
/*        var addr = 'ws://' + document.domain + ':' + location.port + '/api/ws_data'
        var socket = new WebSocket(addr);
        document.getElementById('warning').style.visibility = 'hidden'

        socket.onerror = function(event) {
            console.log('Error detected: ' + event);
        }

        socket.onclose = function() {
            socket = null;
            console.log('Socket closed')
            document.getElementById('warning').style.visibility = 'visible'
        }

        socket.onopen = function(event) {
            console.log('Socket open')
        }

        socket.onmessage = function(msg) {
            console.log("Refreshing data.");
            var data = JSON.parse(msg.data)
            console.log(data)
            drawTradesList(data);
            updateQuotes(data);
        }*/
    }

    function refresh() {
        console.log("Refreshing data.");

        $.getJSON("../api/state", function(data) {
            console.log(data);
            drawTradesList(data);
            updateQuotes(data);

            setTimeout(refresh, 1000);
        }).fail(function(data) {
            if(data['responseJSON'] && data['responseJSON']['error']) {
                console.log(data['responseJSON']['error']);
            } else {
                console.log(data['status'] + ": " + data['statusText']);
            }
            setTimeout(refresh, 1000);
        });
    }

    $("#enterQuotesButton").click(function() {
        buyPrice = $("#buyPriceInput").val();
        sellPrice = $("#sellPriceInput").val();

        if(parseFloat(buyPrice) >= parseFloat(sellPrice)) {
            alert("Your sell price has to be more than your buy price.");
            return;
        }

        if(parseFloat(buyPrice) > 1000000000 || parseFloat(sellPrice) > 1000000000) {
            alert("Prices cannot be larger than 1,000,000,000");
            return;
        }

        if(parseFloat(buyPrice) <= 0 || parseFloat(sellPrice) <= 0) {
            alert("Your prices have to be positive.");
            return;
        }

        console.log("Quotes: {" + buyPrice + "@" + sellPrice + "}");

        $.get("../api/quote?bid=" + buyPrice + "&ask=" + sellPrice,
            function(data, status) {
                console.log("Quotes successfully inserted");
            }
        ).fail(function(data) {
            if(data['responseJSON'] && data['responseJSON']['error']) {
                alert(data['responseJSON']['error']);
            } else {
                alert(data['status'] + ": " + data['statusText']);
            }
        });
    });

    $("#deleteQuotesButton").click(function() {
        console.log("Deleting quotes.");

        $.get("../api/pull",
            function(data, status) {
                console.log("Quotes successfully deleted");
            }
        ).fail(function(data) {
            if(data['responseJSON'] && data['responseJSON']['error']) {
                alert(data['responseJSON']['error']);
            } else {
                alert(data['status'] + ": " + data['statusText']);
            }
        });
    });

    function updateQuotes(state) {
        var myQuotes = state["my_orders"];
        if ("bid" in myQuotes) {
            $("#buy-at-box").removeClass("disabled")
        } else {
            $("#buy-at-box").addClass("disabled")
        }

        if ("ask" in myQuotes) {
            $("#sell-at-box").removeClass("disabled")
        } else {
            $("#sell-at-box").addClass("disabled")
        }
    }

    function drawTradesList(state) {
        var trades = state["trade_list"];
        var tradesText = "";
        trades.forEach(function(trade) {
            if(trade["isme"]) {
                tradesText = (tradesText +
                    trade["buyer"] + " bought @ " +
                    trade["price"] + " from " +
                    trade["seller"] + "\n");
            }
        });

        $("#tradesTextArea").text(tradesText);
        $("#tradesTextArea").scrollTop($("#tradesTextArea")[0].scrollHeight);
    }
});

