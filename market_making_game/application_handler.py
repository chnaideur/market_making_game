#!/usr/bin/env python

import market_state
import datetime
import uuid
import json
import view_controller
import matching_engine
import os

#temporary
import tests.example_state
import random

from bottle import response, request

class Session:
    def __init__(self):
        self.user = None
        self.logged_in = datetime.datetime.now()

class ParameterException(Exception):
    pass

class MarketClosedException(Exception):
    pass

class NotLoggedInException(Exception):
    pass

class ApplicationHandler:
    def __init__(self):
        self.market_state = None
        self.market_state_updated = False
        self.sessions = {}
        self.listFilesLocations = {
            "list-img" : os.path.join("market_making_game", "views", "media", "img")
        }
        self.ws_connections = {}

    def _error(self, code, description):
        response.status = code
        return json.dumps({ 'error' : description, 'query' : request.query.__dict__["dict"] })

    def _ok(self, code = 200, description = "ok"):
        response.status = 200
        return json.dumps({ 'status' : description })

    def _check_for_params(self, params):
        for param in params:
            if param not in request.query:
                raise ParameterException('missing parameter ' + param)

    def _get_param_as_int(self, param):
        self._check_for_params([param])
        try:
            param = int(request.query[param])
        except ValueError:
            raise ParameterException(param + ' is not an integer')
        return param

    def _check_has_market(self):
        if self.market_state is None:
            raise MarketClosedException()

    def _check_logged_in(self, session):
        if session.user is None:
            raise NotLoggedInException()

    def _start(self):
        ticksize = self._get_param_as_int('ticksize')

        if ticksize <= 0:
            raise ParameterException('ticksize out of range')

        self.market_state = market_state.MarketState(ticksize)
        self.sessions = {}
        return self._ok()

    def _login(self, session):
        self._check_for_params(['name'])

        name = request.query['name']
        if len(name) <= 2:
            raise ParameterException('This name is too short')

        if len(name) > 10:
            raise ParameterException('This name is too long')

        self._check_has_market()

        if name in self.market_state.users:
            return self._error(409, 'This name is already in use')

        if session.user is not None:
            return self._error(409, 'You are already logged in as ' + session.user.name)

        session.user = market_state.User(name)
        self.market_state.users[name] = session.user
        return self._ok()

    def _example(self, session):
        self.sessions = {}

        if 'valid' in request.query:
            self.market_state = tests.example_state.example_valid_state()
        else:
            self.market_state = tests.example_state.example_random_state()

        session.user = self.market_state.users[random.choice(self.market_state.users.keys())]

        vc = view_controller.ViewController(self.market_state, session.user)
        return self._ok()

    def _get_state_str(self, session, levels):
        vc = view_controller.ViewController(self.market_state, session.user)
        return json.dumps(vc.getMarketState(levels))

    def _state(self, session):
        self._check_has_market()

        levels = None
        if 'levels' in request.query:
            levels = self._get_param_as_int('levels')

        response.status = 200

        return self._get_state_str(session, levels)

    def _quote(self, session):
        self._check_logged_in(session)
        self._check_has_market()

        vc = view_controller.ViewController(self.market_state, session.user)
        bid_level = vc.roundDownToTick(self._get_param_as_int('bid'))
        ask_level = vc.roundUpToTick(self._get_param_as_int('ask'))

        self._pull(session)
        self.market_state = matching_engine.add_order(self.market_state, market_state.Order(session.user.name, bid_level, True))
        self.market_state = matching_engine.add_order(self.market_state, market_state.Order(session.user.name, ask_level, False))
        self.market_state_updated = True
        return self._ok()

    def _settle(self, session):
        self._check_has_market()
        settlement_price = self._get_param_as_int('price')

        vs = view_controller.ViewController(self.market_state, session.user)
        response.status = 200
        return json.dumps(vs.getSettlementDetails(settlement_price))

    def _pull(self, session):
        self._check_logged_in(session)
        self._check_has_market()
        self.market_state = matching_engine.delete_orders(self.market_state, session.user)
        self.market_state_updated = True
        return self._ok()

    def _get_session(self):
        session_id = request.get_cookie("uuid")

        if session_id is not None and session_id in self.sessions:
            return self.sessions[session_id]

        # Create a new session
        session_id = str(uuid.uuid4())

        thisSession = Session()
        self.sessions[session_id] = thisSession

        response.set_header('Set-Cookie', 'uuid=' + session_id)
        print "Created new session for id: {}".format(session_id)
        return thisSession

    def _list_files(self, name):
        files = os.listdir(self.listFilesLocations[name])
        response.status = 200
        return json.dumps(files)

    def _upload_file(self):
        if not 'upload' in request.files:
            raise ParameterException('missing key "upload')

        upload = request.files.get('upload')

        name, ext = os.path.splitext(upload.filename)
        if ext not in ('.png', '.jpg', '.jpeg'):
            return self._error(500, 'Only .png, .jpg, and .jpeg file extension allowed')

        save_path = os.path.join("market_making_game", "views", "media", "img")
        file_path = "{path}/{file}".format(path=save_path, file="cheese.jpg")

        upload.save(file_path, overwrite=True)
        return self._ok()

    def get_handler(self, name):
        session = self._get_session()

        response.headers['Content-Type'] = 'application/json'
        response.headers['Cache-Control'] = 'no-cache'

        try:
            if name == "start":
                return self._start()
            elif name == "login":
                return self._login(session)
            elif name == "state":
                return self._state(session)
            elif name == "quote":
                return self._quote(session)
            elif name == "pull":
                return self._pull(session)
            elif name == "settle":
                return self._settle(session)
            elif name == "example":
                return self._example(session)
            elif name in self.listFilesLocations:
                return self._list_files(name)

        except ParameterException, e:
            return self._error(400, str(e))
        except MarketClosedException, e:
            return self._error(424, 'Sorry, the market is closed')
        except NotLoggedInException, e:
            return self._error(401, 'You are not logged in')

        response.status = 404
        return json.dumps({ 'error' : 'unknown endpoint' })

    def put_handler(self, name):
        print 'Put: {}'.format(name)

        response.headers['Content-Type'] = 'application/json'
        response.status = 405
        return json.dumps({ 'error' : 'PUT not handled; try GET' })

    def post_handler(self, name):
        print 'Post: {}'.format(name)

        response.headers['Content-Type'] = 'application/json'

        try:
            if name == "upload":
                return self._upload_file()
        except ParameterException, e:
            return self._error(400, str(e))

        response.status = 404
        return json.dumps({ 'error' : 'unknown endpoint' })

    def ws_connection(self, ws):
        session = self._get_session()

        levels = None
        if 'levels' in request.query:
            levels = self._get_param_as_int('levels')

        print ('Received incoming WebSockets connection for session {} '
                'requesting {} levels of the book'.format(request.get_cookie("uuid"), levels))

        self.ws_connections[ws] = (session, levels)

        # Send the current state to the new WS client
        ws.send(self._get_state_str(session, levels))

        while True:
            msg = ws.receive()
            if msg is None:
                break

        del self.ws_connections[ws]

    def send_updated_market_state(self):
        if self.market_state_updated:
            print 'Sending updated state to all clients'
            for ws, (session, levels) in self.ws_connections.items():
                ws.send(self._get_state_str(session, levels))
            self.market_state_updated = False
