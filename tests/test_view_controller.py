
import pytest
from tests.example_state import example_valid_state
from market_making_game.view_controller import ViewController
from market_making_game.market_state import Order

@pytest.fixture()
def vctrl():
    state = example_valid_state()
    return ViewController(state, 'user0')

def test_get_book_no_levels(vctrl):
    vctrl.marketState.book.levels = {}
    book = vctrl.getBook()
    assert book == { 'ticksize': 100, 'levels': [] }

def _convert_levels(levels):
    ret = []
    for level in levels:
        ret.append((level['price'], len(level['bids']), len(level['asks'])))
    return ret

def test_get_book_no_given_levels(vctrl):
    book = vctrl.getBook()
    assert _convert_levels(book['levels']) == [
        (3300, 0, 1),
        (3200, 0, 0),
        (3100, 0, 2),
        (3000, 0, 1),
        (2900, 1, 0),
        (2800, 1, 0),
        (2700, 1, 0),
        (2600, 1, 0)
    ]

def test_get_book_given_levels(vctrl):
    book = vctrl.getBook(5)
    assert _convert_levels(book['levels']) == [
        (3500, 0, 0),
        (3400, 0, 0),
        (3300, 0, 1),
        (3200, 0, 0),
        (3100, 0, 2),
        (3000, 0, 1),
        (2900, 1, 0),
        (2800, 1, 0),
        (2700, 1, 0),
        (2600, 1, 0),
        (2500, 0, 0),
        (2400, 0, 0)
    ]

def test_get_book_cutoff_levels(vctrl):
    book = vctrl.getBook(2)
    assert _convert_levels(book['levels']) == [
        (3200, 0, 0),
        (3100, 0, 2),
        (3000, 0, 1),
        (2900, 1, 0),
        (2800, 1, 0),
        (2700, 1, 0)
    ]
