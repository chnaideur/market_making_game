
import pytest
import copy
import collections

from market_making_game.matching_engine import add_order, delete_orders, get_tob, get_user_pnl
from market_making_game.market_state import Level, Order, Trade

from tests.example_state import example_valid_state

def test_get_tob():
    levels = collections.defaultdict(Level)

    for i in range(100, 105):
        levels[i] = Level()

    levels[100].bids.append(Order('user0', 100, True))
    levels[101].bids.append(Order('user1', 101, True))
    levels[103].asks.append(Order('user2', 103, False))
    levels[104].asks.append(Order('user3', 104, False))

    (best_bid, best_ask) = get_tob(levels)
    assert best_bid == 101 and best_ask == 103

@pytest.mark.parametrize('order,trade_price', [
    (Order('user4', 2950, True),  None), # No trade should happen

    (Order('user4', 2900, True),  None),
    (Order('user4', 3000, True),  3000),
    (Order('user4', 3100, True),  3000),
    (Order('user4', 3200, True),  3000),

    (Order('user4', 3000, False), None),
    (Order('user4', 2900, False), 2900),
    (Order('user4', 2800, False), 2900),
    (Order('user4', 2850, False), 2900),
])
def test_add_order(order, trade_price):
    old_ms = example_valid_state()
    previous_trade_count = len(old_ms.trades)
    market_state = add_order(example_valid_state(), order)

    if trade_price:
        level = market_state.book.levels[trade_price]
        orders_on_side = level.asks if order.isBid else level.bids

        old_level = old_ms.book.levels[trade_price]
        old_orders_on_side = old_level.asks if order.isBid else old_level.bids

        # Check that the trade has been registered
        assert len(market_state.trades) == previous_trade_count + 1
        assert market_state.trades[-1].price == trade_price

        # Check that the standing order has been deleted
        assert ((not trade_price in orders_on_side) or
                (len(orders_on_side) + 1 == len(old_orders_on_side)))

        # Check that the order is not present in the user orders list
        assert len(market_state.users['user4'].orders) == 0

    else:
        level = market_state.book.levels[order.price]
        orders_on_side = level.bids if order.isBid else level.asks

        old_level = old_ms.book.levels[order.price]
        old_orders_on_side = old_level.bids if order.isBid else old_level.asks

        assert len(old_orders_on_side) + 1 == len(orders_on_side)

        user = market_state.users['user4']
        assert user.orders[0] == order

def test_delete_orders():
    ms = example_valid_state()
    ms = delete_orders(ms, ms.users['user1'])

    assert not 2700 in ms.book.levels
    assert len(ms.book.levels[3100].bids) == 0
    assert len(ms.book.levels[3100].asks) == 1

def test_get_user_pnl():
    ms = example_valid_state()
    pnl = get_user_pnl(ms, 900)
    expected_pnl = {
        'user0': -4850,
        'user1': -100,
        'user2': 3250,
        'user3': 1700,
        'user4': 0,
        'user5': 0,
        'user6': 0,
        'user7': 0,
        'user8': 0,
        'user9': 0,
    }
    assert pnl == expected_pnl
