#!/usr/bin/env python

import random

from market_making_game.market_state import User, Trade, Order, Level, Book, MarketState

def example_random_state():
    ticksize = 10
    minprice = 2500
    maxprice = 3000
    m = MarketState(ticksize)

    for i in range(10):
        user_name = "user" + str(i)
        m.users[user_name] = User(user_name)

    for u in m.users.itervalues():
        for isBid in [True, False]:
            price = random.randint(minprice/ticksize, maxprice/ticksize) * ticksize
            order = Order(u.name, price, isBid)
            u.orders.append(order)
            if isBid:
                m.book.levels[price].bids.append(order)
            else:
                m.book.levels[price].asks.append(order)

    for i in range(20):
        price = random.randint(minprice/ticksize, maxprice/ticksize) * ticksize
        buyer =  m.users[random.choice(list(m.users))]
        seller = m.users[random.choice(list(m.users))]
        while (buyer == seller):
            seller = m.users[random.choice(list(m.users))]

        trade = Trade(buyer.name, seller.name, price)
        m.trades.append(trade)

    return m

def example_valid_state():
    ticksize = 100
    m = MarketState(ticksize)

    for i in range(10):
        user_name = "user" + str(i)
        m.users[user_name] = User(user_name)

    def add_order(order):
        if order.isBid:
            m.book.levels[order.price].bids.append(order)
        else:
            m.book.levels[order.price].asks.append(order)

        m.users[order.user].orders.append(order)

    def add_trade(trade):
        m.trades.append(trade)

    # Populate with existing orders
    add_order(Order('user0', 2600, True))
    add_order(Order('user1', 2700, True))
    add_order(Order('user2', 2800, True))
    add_order(Order('user3', 2900, True))
    add_order(Order('user5', 3000, False))
    add_order(Order('user6', 3100, False))
    add_order(Order('user1', 3100, False))
    add_order(Order('user8', 3300, False))

    add_trade(Trade('user0', 'user1', 2400));
    add_trade(Trade('user1', 'user2', 2500));
    add_trade(Trade('user0', 'user2', 2550));
    add_trade(Trade('user0', 'user3', 2600));

    return m
