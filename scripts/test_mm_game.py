#! /usr/bin/env python

import requests
import sys
import time
import random

ip = 'http://' + sys.argv[1]

def get_static(page):
    r = requests.get(ip + page)
    print 'Status code: {}'.format('OK' if r.status_code == 200 else str(r.status_code))

static_pages = [
    '',
    '/media/bootstrap-4.1.1.min.css',
    '/media/app.css',
    '/media/jquery-3.1.1.min.js',
    '/media/popper-1.14.3.min.js',
    '/media/bootstrap-4.1.1.min.js',
    '/media/login.js'
]

def user_thread():
    for page in static_pages:
        get_static(page)

    name = "User" + str(random.choice(range(10000)))

    session = requests.session()
    p = session.get(ip + '/api/login?name=' + name)
    print 'cookies', requests.utils.dict_from_cookiejar(session.cookies)

    while True:
        previous_time = time.time()

        session.get(ip + '/api/state')

        rand = random.choice(range(30))
        rand_action_delay = (1000 - random.choice(range(1000))) / 1000.0
        if rand in [ 0, 1 ]:
            time.sleep(rand_action_delay)
            bid_price = random.choice(range(1, 1000000))
            ask_price = random.choice(range(bid_price, 1000010))
            session.get(ip + '/api/quote?bid=' + str(bid_price) + '&ask=' + str(ask_price))

        elif rand == 2:
            time.sleep(rand_action_delay)
            session.get(ip + '/api/pull')

        sleep_time = previous_time + 1 - time.time()
        sleep_time = max(0.1, sleep_time)
        time.sleep(sleep_time)


import threading

threads = []
for i in range(60):
    t = threading.Thread(target=user_thread)
    t.daemon = True
    threads.append(t)
    t.start()
    time.sleep(1)

while(True):
    pass
